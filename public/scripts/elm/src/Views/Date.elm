module Views.Date exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (..)
import Msg exposing (..)


root : DateViewModel -> Html Msg
root model =
    div [ class "main-collapsing" ]
        [ div [ class "flex-row" ]
              [ h2 [ class "flex-1 h2-date" ] [ text model.date.dateLong ]
              , div [] [ closeButton model ] --TODO
              ]
        , healthSection model
        ]


--------------------------------------
closeButton : DateViewModel -> Html Msg
closeButton model =
    button [ class "btn", onClick (UpdateDate Nothing) ] [ text "❌" ]

healthSection : DateViewModel -> Html Msg
healthSection model =
    div [ class "div-date-section" ]
        ( if  model.healthMenuOpen
          then [ div [ class "flex-row" ]
                     [ h3 [ class "h3-date-section" ] [ text "Health" ]
                     , button [ class "btn", onClick (OpenHealthMenu False) ]
                         [ text "❌"
                         ]
                     ]
               , button [ class "btn" ] [ text "Menstrual Entry" ]
               ] 
          else [ div [ class "flex-row" ]
                     [  h3 [ class "h3-date-section" ] [ text "Health" ]
                     , button [ class "btn", onClick (OpenHealthMenu True) ]
                         [ text "➕"
                         ]
                     ]
               ]
        )
