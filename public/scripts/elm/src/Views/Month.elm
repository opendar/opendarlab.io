module Views.Month exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (..)
import Msg exposing (..)
import Views.DateSquare exposing (root)

root: Models.MonthModel -> Models.DateModel-> Html Msg
root month today=
    div [ class "main-collapsing" ]
        [ monthTopline month
        , monthTable month today
        ]
        
-------------------------------------------------------------------------------

monthTable : Models.MonthModel -> Models.DateModel -> Html Msg
monthTable month today =
    table [ class "table-month" ]
        [ case List.head month.weeks of
              Just week -> thead [] (List.map (\day -> monthTh day) week)
              Nothing -> thead [] [th [] [text "Exceptional state: No weeks."]]
        , monthTbody month today
        ]

monthTbody : Models.MonthModel -> Models.DateModel -> Html Msg
monthTbody month today =
    tbody [] (List.map (\week -> monthTr week month.index today) month.weeks)
        
monthTh : DateModel -> Html Msg
monthTh day =
    th []
        [ span [] [ text day.weekdayShort ]
        ]
        
monthTopline : Models.MonthModel -> Html Msg
monthTopline month =
    div [ class "flex-row" ]
        [ button [ class "btn", onClick (DecrementMonth month) ] [ text "←" ]
        , h2 [ class "flex-1 h2" ]
            [ text month.name
            , text " "
            , String.fromInt month.year |> text
            ]
        , button [ class "btn", onClick (IncrementMonth month) ] [ text "→" ]
        ]

monthTr : List Models.DateModel -> Int -> Models.DateModel -> Html Msg
monthTr days month today =
    tr [] (List.map (\day -> Views.DateSquare.root day month today) days)
            
