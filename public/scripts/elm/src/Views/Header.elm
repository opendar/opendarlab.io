module Views.Header exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Msg exposing (Msg)


root: Html Msg
root = header [ class "header" ]
       [ h1 [ class "h1" ] [ text "Opendar" ]
       , small [ class "header-small" ] [ text "privacy-centric calendar" ]
       ]
