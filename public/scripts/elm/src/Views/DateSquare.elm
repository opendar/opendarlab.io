module Views.DateSquare exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models 
import Msg exposing (..)

root : Models.DateModel -> Int -> Models.DateModel-> Html Msg
root model month today =
    td [ class "td-date"
       , attribute "role" "button"
       , onClick (UpdateDate (Just model))
       ]
    [ span [ class (dateClass model month today) ]  [ text model.day ]
    ]

-------------------------------------------------------------------------------
dateClass : Models.DateModel -> Int -> Models.DateModel -> String
dateClass model month today =
    if sameDate model today
    then "span-date span-date-today"
    else if model.month == month
         then "span-date span-date-month"
         else "span-date"



sameDate : Models.DateModel -> Models.DateModel -> Bool
sameDate model today =
    model.year == today.year
        && model.month == today.month
        && model.day == today.day
