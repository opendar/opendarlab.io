module Views.Main exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (..)
import Msg exposing (..)
import Views.Date
import Views.Month 


root: Model -> Html Msg
root model =
    main_ [ class "main flex-row" ]
        [ case model.month of
              Just month -> Views.Month.root month model.today
              Nothing -> text "Loading..."
        , case model.date of
              Just date -> Views.Date.root date
              Nothing -> text ""
        ]

-------------------------------------------------------------------------------
