module Main exposing (main)

import Browser
import Browser.Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder)
import Models exposing (Model)
import Msg exposing (..)
import Ports
import Views.Header
import Views.Main
import Url

main : Program () Model Msg
main = Browser.application
       { init = init
       , subscriptions = subscriptions
       , update = update
       , view = view
       , onUrlRequest = UpdateUrl  
       , onUrlChange = UpdatePage
       }

------------------------
init : flags -> Url.Url -> Browser.Navigation.Key -> (Model, Cmd Msg)
init _ _ key = ( Models.init key
         , Cmd.none
         )

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Ports.updateMonth UpdateMonth
              , Ports.updateToday UpdateToday
              , Ports.updateMenstrualEntries UpdateMenstrualEntries
              ]

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        DecrementMonth month -> (model, Ports.decrementMonth month)
        IncrementMonth month -> (model, Ports.incrementMonth month)
        OpenHealthMenu bool ->
            case model.date of
                Just date -> { date | healthMenuOpen = bool }
                             |> \d -> ({ model | date = Just d }, Cmd.none)
                Nothing -> (model, Cmd.none)
        UpdateDate maybe ->
            case maybe of
                Just date -> Just (Models.dateToViewModel date)
                             |> \d -> ({ model | date = d }, Cmd.none)
                Nothing -> ({ model | date = Nothing }, Cmd.none)
        UpdateMenstrualEntries value -> 
            case Decode.decodeValue Models.menstrualEntriesDecoder value of
                Ok entries -> Models.updateMenstrualEntries model entries
                           |> \m -> (m, Cmd.none)
                Err error -> Debug.toString error |> Debug.log
                             |> \str -> (model, Cmd.none)
        UpdateMonth value -> 
            case Decode.decodeValue Models.decoder value of
                Ok month -> ({ model | month = Just month }, Cmd.none)
                Err error -> Debug.toString error |> Debug.log
                             |> \str -> (model, Cmd.none)
        UpdatePage url -> (model, Cmd.none) --TODO
        UpdateToday value ->
            case Decode.decodeValue Models.dateDecoder value of
                Ok today -> ({ model | today = today }, Cmd.none)
                Err error -> (model, Cmd.none)
        UpdateUrl request ->
            case request of
                Browser.Internal url ->
                    Url.toString url
                          |> \str ->
                              (model, Browser.Navigation.pushUrl model.key str)
                Browser.External href -> (model, Browser.Navigation.load href)


type alias Document msg =
    { title : String
    , body : List (Html msg)
    }
    
view : Model -> Document Msg
view model =
    { title = "Opendar | privacy-centric calendar"
    , body = [
           div [ class "container" ]
               [ Views.Header.root
               , Views.Main.root model
               ]
          ]
    }

{---------------------------helpers----------------------------}
        
