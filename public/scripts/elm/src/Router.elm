{- Not deleting this, but not using it either, for now. I'd originally 
thought maybe I'd
do a lot of state management through the URL. The benefit of that is 
it would allow users to save state using bookmarks and links, but 
  1. I don't think a lot of users are going to bookmark things like indiviual
days or months.
  2. It's really going to complicate my design. Ultimately, there's so much 
state and there would be a ton of ugly query string parameters. This is all
very unnecessary. 
-}

import Model exposing (Page)
import Url.Parser exposing (Parser, (</>), int, map, oneof, s, string)

routeParser : Parser (Page -> a)
routeParser =
    oneOf
    [ map Home top ]
