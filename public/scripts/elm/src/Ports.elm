port module Ports exposing (..)

import Json.Decode exposing ( Value )
import Models exposing (MonthModel)

port decrementMonth : MonthModel -> Cmd msg

port incrementMonth : MonthModel -> Cmd msg

port updateMenstrualEntries : (Value -> msg) -> Sub msg

port updateMonth : (Value -> msg) -> Sub msg

port updateToday : (Value -> msg) -> Sub msg

                   
