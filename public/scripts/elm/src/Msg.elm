module Msg exposing (..)

import Browser
import Json.Encode
import Models exposing (DateModel, MonthModel)
import Url

type Msg = DecrementMonth MonthModel
         | IncrementMonth MonthModel
         | OpenHealthMenu Bool
         | UpdateDate (Maybe DateModel)
         | UpdateMenstrualEntries Json.Encode.Value
         | UpdateMonth Json.Encode.Value
         | UpdatePage Url.Url
         | UpdateToday Json.Encode.Value
         | UpdateUrl Browser.UrlRequest

