module Models exposing (..) --todo

import Browser.Navigation
import Json.Decode exposing (Decoder, float, int, list, string)
import Json.Decode.Pipeline

type alias DateViewModel =
    { date : DateModel
    , healthMenuOpen : Bool
    }

type alias Model =
    { data: DataModel
    , date : Maybe DateViewModel --date open on the second panel
    , key: Browser.Navigation.Key
    , month: Maybe MonthModel
    , today: DateModel
    , screen: Screen
    }

type alias DataModel =
    { menstrualEntries: List MenstrualEntry
    }

type alias DateModel =
    { posix: Float
    , weekdayLong: String
    , weekdayShort: String
    , dateLong: String
    , day: String
    , month: Int
    , year: Int
    }

type alias ImDate =
    { milliseconds: Float
    }

type alias MenstrualEntry =
    { date: ImDate
    , flow: Int
    , id: String
    }

type alias MonthModel = 
    { index: Int {- 0-11 -}
    , name: String
    , year: Int
    , weeks: List (List DateModel) 
    }

type Screen = Home

{-decoders-}

dateDecoder : Decoder DateModel
dateDecoder = Json.Decode.succeed DateModel
            |> Json.Decode.Pipeline.required "posix" float
            |> Json.Decode.Pipeline.required "weekdayLong" string
            |> Json.Decode.Pipeline.required "weekdayShort" string
            |> Json.Decode.Pipeline.required "dateLong" string
            |> Json.Decode.Pipeline.required "day" string
            |> Json.Decode.Pipeline.required "month" int
            |> Json.Decode.Pipeline.required "year" int

decoder : Decoder MonthModel
decoder = Json.Decode.succeed MonthModel
        |> Json.Decode.Pipeline.required "index" int
        |> Json.Decode.Pipeline.required "name" string
        |> Json.Decode.Pipeline.required "year" int
        |> Json.Decode.Pipeline.required "weeks" (list (list dateDecoder))

imDateDecoder: Decoder ImDate
imDateDecoder = Json.Decode.succeed ImDate
              |> Json.Decode.Pipeline.required "milliseconds" float

----
dateToViewModel : DateModel -> DateViewModel
dateToViewModel date =
    { date = date
    , healthMenuOpen = False
    }

init : Browser.Navigation.Key -> Model
init key =
    { data = initDataModel
    , date = Nothing
    , key = key
    , month = Nothing
    , today = initDateModel
    , screen = Home
    }

menstrualEntriesDecoder: Decoder (List MenstrualEntry)
menstrualEntriesDecoder = Json.Decode.list menstrualEntryDecoder

updateMenstrualEntries: Model -> List MenstrualEntry -> Model
updateMenstrualEntries m entries =
    m.data
        |> \data -> { data | menstrualEntries = entries }
        |> \d -> { m | data = d }

-------------------------------------------------------------------------------
initDataModel : DataModel
initDataModel =
    { menstrualEntries = []
    }
    
initDateModel : DateModel
initDateModel =
    { posix = 0
    , weekdayLong = ""
    , weekdayShort = ""
    , dateLong = ""
    , day = ""
    , month = 0
    , year = 0
    }

menstrualEntryDecoder: Decoder MenstrualEntry
menstrualEntryDecoder = Json.Decode.succeed MenstrualEntry
                        |> Json.Decode.Pipeline.required "date" imDateDecoder
                        |> Json.Decode.Pipeline.required "flow" int
                        |> Json.Decode.Pipeline.required "string" string
