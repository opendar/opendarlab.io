type t;

@get external crypto: Dom.window => t = "crypto";

/* @get external subtle: t => SubtleCrypto.t = "subtle"; */

@send external getRandomValues:
(t, Js.TypedArray2.Uint8Array.t) => Js.TypedArray2.Uint8Array.t
  = "getRandomValues";

@send external randomUUID: t => string = "randomUUID";
