type t = {
  index: float, /* 0-11 */
  name: string,
  weeks: array<array<DateModel.t>>,
  year: float,
};

let rec firstDisplayDay = (date: ImDate.t): ImDate.t =>
  if ImDate.getDay(date) == 0. { date }
  else { ImDate.previous(date) -> firstDisplayDay };

let rec weekHelper =
  (weekSoFar: array<DateModel.t>, current: ImDate.t): array<DateModel.t> =>
  if Js.Array.length(weekSoFar) < 7 {
    weekHelper(Js.Array.concat([ DateModel.fromImDate(current) ], weekSoFar),
               ImDate.next(current));
  }
  else { weekSoFar };

let week = (date: ImDate.t): array<DateModel.t> => weekHelper([], date);

let rec sixWeeksHelper = (setSoFar: array<array<DateModel.t>>,
                       firstDay: ImDate.t):
                  array<array<DateModel.t>> => {
  if Js.Array.length(setSoFar) < 6 {
    sixWeeksHelper(Js.Array.concat([ week(firstDay) ], setSoFar),
                       ImDate.addDays(firstDay, 7.));
  }
    else { setSoFar };
};

let sixWeeks = (firstDay: ImDate.t): array<array<DateModel.t>> =>
  sixWeeksHelper([], firstDay);

let fromDate = (date: ImDate.t): t => {
  index: ImDate.getMonth(date),
  name: FormatImDate.getMonthText(date),
  year: ImDate.getFullYear(date),
  weeks: sixWeeks(firstDisplayDay(date))
};

let fromYearAndMonth = (year: float, month: float): t =>
  ImDate.fromYM(year, month) -> fromDate;
