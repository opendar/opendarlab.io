open! Belt.Float;
open Elm;

/* setup: simple JS dom interop */
@val @scope("document")
external getElementById: string => Dom.element = "getElementById";

@val external window: Dom.window = "window";

module Ports = {
  type t = {
    decrementMonth: Elm.subscribable<MonthModel.t>,
    incrementMonth: Elm.subscribable<MonthModel.t>,
    saveMenstrualEntry: Elm.subscribable<MenstrualEntry.t>,
    updateMenstrualEntries: Elm.sendable<array<MenstrualEntry.t>>,
    updateMonth: Elm.sendable<MonthModel.t>,
    updateToday: Elm.sendable<DateModel.t>
  }
};

let updateMenstrualEntries =
  (app: Elm.app<Ports.t>,
   entries: array<MenstrualEntry.t>):Js.Promise.t<unit> => {
     app.ports.updateMenstrualEntries
       -> Elm.send(entries)
       |> Js.Promise.resolve;
   };

let uuid = (): string  => window -> Crypto.crypto -> Crypto.randomUUID;

let initialize = (now: ImDate.t, db: IndexedDB.IDBDatabase.t):
    Elm.app<Ports.t> => {
      Js.Console.log("initializing elm app...");
      let app: Elm.app<Ports.t> =
        Elm.Main.init({node: getElementById("elm-target")});
      Js.Console.log("Elm app initialized.");
      app.ports.updateMonth
        -> Elm.send(MonthModel.fromYearAndMonth(
          ImDate.getFullYear(now), ImDate.getMonth(now)
        ));

      app.ports.updateToday -> Elm.send(DateModel.fromImDate(now));

      app.ports.decrementMonth
        -> Elm.subscribe(month => {
          app.ports.updateMonth
            -> Elm.send(MonthModel.fromYearAndMonth(month.year, month.index - 1.0));
        });

      app.ports.incrementMonth
        -> Elm.subscribe(month => {
          app.ports.updateMonth
            -> Elm.send(MonthModel.fromYearAndMonth(month.year, month.index + 1.0));
        });

      app.ports.saveMenstrualEntry
        -> Elm.subscribe(entry => {
          let _ = Database.stores.menstrualEntries
              -> Reap.Store.add(db, {...entry, id: uuid() });
        });

      app;
    };
