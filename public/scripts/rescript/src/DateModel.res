/* represents one calendar day */

type t = {
  posix: float,
  weekdayLong: string,
  weekdayShort: string,
  dateLong: string,
  day: string, //2-digit
  month: float,
  year: float
};

let fromImDate = (date: ImDate.t): t => {
  posix: date.milliseconds,
  weekdayLong: FormatImDate.getWeekdayLong(date),
  weekdayShort: FormatImDate.getWeekdayShort(date),
  dateLong: FormatImDate.getDateLong(date),
  day: FormatImDate.getDay(date),
  month: ImDate.getMonth(date),
  year: ImDate.getFullYear(date)
};


  
