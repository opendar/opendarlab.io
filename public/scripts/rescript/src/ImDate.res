/* an immutable date and time library */

open Js;
open! Belt.Float;

type t = {
  milliseconds: float
};

/* constructors */

let fromDate = (date: Date.t): t => {
  milliseconds: Date.getTime(date)
};

let fromFloat = (milliseconds: float): t => {
  milliseconds: milliseconds
};

let fromYM = (year: float, month: float): t =>
  Date.makeWithYM(~year=year, ~month=month, ()) -> Date.getTime -> fromFloat;

/* methods */
let addDays = (t: t, days: float): t => {
  Date.fromFloat(t.milliseconds)
    -> Date.setDate(Date.getDate(Date.fromFloat(t.milliseconds)) + days)
    -> fromFloat
};


let getDay = (t: t): float => Date.fromFloat(t.milliseconds) -> Date.getDay;

let getDate = (t: t): float => Date.fromFloat(t.milliseconds) -> Date.getDate;

let getFullYear = (t: t): float =>
  Date.fromFloat(t.milliseconds)
  -> Date.getFullYear;

let getMonth = (t: t): float =>
  Date.fromFloat(t.milliseconds)
  -> Date.getMonth;

let next = (t: t): t => addDays(t, 1.);

let now = (): t => { milliseconds: Date.now() };

let previous = (t: t): t => addDays(t, -1.);
