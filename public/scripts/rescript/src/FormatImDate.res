open Js;

type localeOptions = {
  weekday: option<string>,
  year: option<string>,
  month: option<string>,
  day: option<string>
};

let localeInit = {
  weekday: None,
  year: None,
  month: None,
  day: None
};

@bs.send external toLocaleString: (Date.t, string, localeOptions) => string
  = "toLocaleString";

let getDay = (t: ImDate.t): string =>
  Date.fromFloat(t.milliseconds)
  -> toLocaleString("default", {...localeInit, day: Some("2-digit")});

let getDateLong = (t: ImDate.t): string =>
  Date.fromFloat(t.milliseconds)
  -> toLocaleString("default", { weekday: Some("long"),
                                 year: Some("numeric"),
                                 month: Some("long"),
                                 day: Some("numeric")
});                                

let getMonthText = (t: ImDate.t): string => {
  Date.fromFloat(t.milliseconds)
    -> toLocaleString("default", {...localeInit, month: Some("long") });
};

let getWeekdayLong = (t: ImDate.t): string => 
  Date.fromFloat(t.milliseconds)
  -> toLocaleString("default", {...localeInit, weekday: Some("long") });

let getWeekdayShort = (t: ImDate.t): string =>  
  Date.fromFloat(t.milliseconds)
  -> toLocaleString("default", {...localeInit, weekday: Some("short") }); 
