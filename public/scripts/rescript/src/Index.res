@val external window: Dom.window = "window";

let onDBOpen = (db: IndexedDB.IDBDatabase.t) =>
    -> Js.Date.now()
    -> ImDate.fromFloat
    -> ElmInterop.initialize(db);

Database.create(onDBOpen);

