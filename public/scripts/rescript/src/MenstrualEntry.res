type t = {
  date: ImDate.t,
  flow: int, //scale 1-5
  id: string,
  updated: ImDate.t
};
