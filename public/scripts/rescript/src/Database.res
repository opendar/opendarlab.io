type storeType = {
  menstrualEntries: Reap.Store.t<MenstrualEntry.t>
};

let stores: storeType = {
  menstrualEntries: {
    indices: [{
      name: "date",
      parameters: { unique: false, locale: None },
      path: Reap.Path.toPath((m: MenstrualEntry.t) => m.date),
      version: 1
    }],
    key: m => m.id,
    name: "menstrual-entries",
    version: 1
  }
};

let create = (onCreated: IndexedDB.IDBDatabase.t => 'tnext): unit => {
  databaseName: "opendar",
  version: 1,
  stores: [ stores.menstrualEntries -> Reap.Store.toConfig ],
  onCreated: onCreated,
  onUpgraded: _ => Js.Console.log("Database upgraded."),
  onCreationError: _ => Js.Console.error("Database error.")
} -> Reap.Database.create;
  
    
