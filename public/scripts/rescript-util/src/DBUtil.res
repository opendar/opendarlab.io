open IndexedDB;

let toPromise = (request: IDBRequest.t<'t>): Js.Promise.t<'t> =>
  Js.Promise.make((~resolve, ~reject) => {
    request ->
      IDBRequest.set_onsuccess(e =>
        resolve(. e
                -> RequestSuccessEvent.target
                -> IDBRequest.result)
      )
  });
