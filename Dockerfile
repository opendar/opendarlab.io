FROM node:latest
WORKDIR /usr/src/app
COPY . .

# RUN npm i -g elm
RUN npm i -g http-server

WORKDIR /usr/src/app/public/scripts/rescript
RUN npm i
RUN npm run build

WORKDIR /usr/src/app/public/scripts/
RUN npm i

WORKDIR /usr/src/app/public/scripts/elm
RUN npx elm make src/Main.elm --output=index.js

WORKDIR /usr/src/app/public/
EXPOSE 8080
ENTRYPOINT ["http-server", "-p", "8080"]
