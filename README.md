# README

Opendar is an entirely non-monetized, privacy focused calendar app with health
tracking. 

## Build and Run

It's a simple static site--no back end. Use whatever server you want. I use
the `http-server` npm package using `sudo npm i -g http-server`. 

```sh
cd public
./build && http-server
```

## Contribution

## High-level overview

Reconciling different sources of truth is complicated because there are two,
equal sources of truth: Local data and remote data. On top of this, the view
data will often include or create user data. And on top of all of this, the
view, the IndexedDb, and the remote data are all asynchronously read from and
written to. 

To simplify this, I'm introducing a *fourth* source of truth, the in-memory
data state. 

* On page start, the in-memory data state will load all data from the 
local IndexedDb. 
* After the in-memory data state has loaded, it will update the Elm view with
the data from the database. 
* At the same time, the in-memory data state asynchronously reconciles with
the remote data. 
* After the reconciliation, the remote data state writes itself to the 
local IndexedDb--now the IndexedDb is reconciled with the remote data. 
* At the same time, the in memory data state pushes the reconciled state onto
the Elm view. 
* On each update from the Elm view, the in memory data state writes itself to
both the remote and local data. 

SUPER simple, right? 

Basically, having an in-memory, synchronous copy of the data is a good way to
prevent race conditions, and I don't think the data will ever get big enough to
cause meaningful memory consumption. (IndexedDB is capped at 40mb anyway, and
I don't know what to do about that.)

## TODO

